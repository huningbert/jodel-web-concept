# jodel-web-concept
Concept for a possible web-implementation of the popular hyperlocal social network Jodel using Firebase &amp; React.

## TO-DO
- [x] <del>add API for posting/receiving jodels (text only)</del>
- [ ] add authentication
  - [ ] first authentication via e-mail
  - [ ] later change to authentication via phone
- [ ] add post interaction
  - [ ] add commenting on posts
  - [ ] add post editing
  - [ ] add voting on posts/comments (upvote/downvote)
- [ ] add posting interface
  - [ ] add image upload
